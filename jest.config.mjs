import { jest } from './node_modules/quickstartconfig/index.js';

export default {
	...jest,
	testEnvironment: `node`,
	preset: 'ts-jest',
};
