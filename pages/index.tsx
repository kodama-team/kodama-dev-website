import * as React from 'react';
import Head from 'next/head';
import { ALL_ICONS } from '../lib/icons';
import * as config from '../config';
import type { NextPage } from 'next';

const MAX_ICONS = 40; // Maximum number of icons to load.
const MAX_GRID_CELLS = 9 * 7; // Maximum number of grid cells that need filling with icons.

interface BgIconSorted {
	iconIndex: number;
	weight: number;
}

/**
 * Component properties.
 */
export interface IProps {
	bgIcons: BgIconSorted[];
}

/**
 * Page component.
 */
const Page: NextPage<IProps> = ({ bgIcons }: IProps) => {
	return (
		<>
			<Head>
				{/* General */}
				<title>
					{config.constants.SITE_NAME_FULL} - {config.constants.TAG_LINE}
				</title>
				<meta name="description" content="Learn to code remotely - change your career and change your life." />
			</Head>
			<div className="relative flex h-full w-full p-4">
				<div
					className={`absolute top-0 left-0 z-0 grid h-full w-full auto-rows-[0] grid-cols-4 grid-rows-3 overflow-hidden sm:grid-cols-5 md:grid-cols-6 lg:grid-cols-7 xl:grid-cols-8 2xl:grid-cols-9 h-sm:grid-rows-4 h-md:grid-rows-5 h-lg:grid-rows-6 h-xl:grid-rows-7`}
				>
					{bgIcons.map(({ iconIndex }, index) => {
						const BgIcon = ALL_ICONS[iconIndex];
						return <BgIcon key={index} className="m-auto h-10 w-10 fill-[#FEFEEC] opacity-10" />;
					})}
				</div>
				<div className="z-10 m-auto text-center text-[#FEFEEC] shadow-md">
					<div className="mb-4 font-logo text-6xl opacity-95 sm:text-8xl">{config.constants.SITE_NAME}</div>
					<div className="text-lg font-light opacity-80 sm:text-2xl">{config.constants.TAG_LINE}</div>
					<div className="text-md mt-20 mb-2 opacity-80 sm:text-xl">Get notified when we launch:</div>
					<form
						action="https://dev.us13.list-manage.com/subscribe/post?u=2db617e2772c1c3dbc947a33a&amp;id=b876b8938b"
						method="post"
						className="block"
						id="mc-embedded-subscribe-form"
						name="mc-embedded-subscribe-form"
						target="_blank"
					>
						<input
							type="email"
							name="EMAIL"
							id="mce-EMAIL"
							required
							placeholder="email@example.com"
							className="w-64 rounded-md border-2 border-solid border-[#FEFEEC] px-3 py-1 text-base text-slate-700 opacity-90 hover:opacity-100 focus:opacity-100 sm:text-lg"
						/>
						<input type="hidden" name="tags" value="6661847,6661851" />
						<div className="absolute -left-[5000px]" aria-hidden="true">
							<input type="text" name="b_2db617e2772c1c3dbc947a33a_b876b8938b" tabIndex={-1} />
						</div>
						<input
							type="submit"
							value="Subscribe"
							name="subscribe"
							id="mc-embedded-subscribe"
							className="mx-auto mt-4 block cursor-pointer rounded-md border-2 border-solid px-3 py-1 text-base opacity-90 hover:opacity-100 sm:mt-0 sm:ml-2 sm:inline-block sm:text-lg"
						/>
					</form>
				</div>
			</div>
		</>
	);
};

/**
 * Generate component props on page load.
 */
export async function getServerSideProps() {
	const bgIcons = ALL_ICONS.map((_, iconIndex) => ({ iconIndex, weight: Math.random() }))
		.sort((itemA, itemB) => itemA.weight - itemB.weight)
		.slice(0, MAX_ICONS);

	if (bgIcons.length < MAX_GRID_CELLS) bgIcons.push(...bgIcons);

	return { props: { bgIcons } };
}

export default Page;
