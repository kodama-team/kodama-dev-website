import * as React from 'react';
import { Html, Head, Main, NextScript } from 'next/document';
import * as config from '../config';

const NextDocument = (): React.ReactElement => {
	return (
		<Html lang="en-GB">
			<Head>
				{/* General */}
				<meta name="author" content={config.constants.SITE_NAME_FULL} />
				<link rel="shortcut icon" type="image/jpg" href="/favicon.jpg" key="favicon" />

				{/* Apple */}
				<meta name="apple-mobile-web-app-capable" content="yes" />
				<meta name="apple-mobile-web-app-status-bar-style" content="black" />
			</Head>
			<body>
				<Main />
				<NextScript />
			</body>
		</Html>
	);
};

export default NextDocument;
