import * as React from 'react';
import { RecoilRoot } from 'recoil';
import '../styles/globals.css';
import '../styles/fonts.css';
import type { AppProps } from 'next/app';
// import Head from 'next/head';

const NextApp = ({ Component, pageProps }: AppProps): React.ReactElement => {
	return (
		<RecoilRoot>
			{/* <Head></Head> */}
			<Component {...pageProps} />
		</RecoilRoot>
	);
};

export default NextApp;
