const { eslintReact } = require('./node_modules/quickstartconfig');

module.exports = {
	...eslintReact,
	rules: {
		...eslintReact.rules,
		'react-hooks/exhaustive-deps': [
			'warn',
			{
				additionalHooks: '(useRecoilCallback|useRecoilTransaction_UNSTABLE)',
			},
		],

		'@typescript-eslint/naming-convention': [
			...baseConfig.rules['@typescript-eslint/naming-convention'],
			{
				selector: 'variable',
				modifiers: ['const'],
				types: ['function'],
				format: ['PascalCase', 'camelCase'],
			},
			{
				selector: 'parameter',
				// modifiers: ['destructured'],
				format: ['PascalCase', 'camelCase'],
			},
		],
	},
};
