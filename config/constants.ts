export const SITE_NAME = `Kodama`;
export const SITE_NAME_FULL = `Kodama.dev`;
export const TAG_LINE = `Coding Course • Podcast • Nerd Swag`;
