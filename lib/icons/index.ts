import * as icons from './list'; // Must first import so we can convert it to an array for exporting.
export * from './list';

export const ALL_ICONS: any[] = Object.values(icons).map(icon => icon.default);
