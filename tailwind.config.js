module.exports = {
	content: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
	theme: {
		extend: {
			fontFamily: {
				logo: ['kaushan_script'],
			},
			borderWidth: {
				1: `1px`,
			},
			gridTemplateRows: {
				7: `repeat(7, minmax(0, 1fr));`,
				8: `repeat(8, minmax(0, 1fr));`,
				9: `repeat(9, minmax(0, 1fr));`,
				10: `repeat(10, minmax(0, 1fr));`,
			},
			screens: {
				'h-sm': { raw: '(min-height: 300px)' },
				'h-md': { raw: '(min-height: 500px)' },
				'h-lg': { raw: '(min-height: 700px)' },
				'h-xl': { raw: '(min-height: 900px)' },
				'h-2lx': { raw: '(min-height: 1100px)' },
			},
		},
	},
	plugins: [require('@tailwindcss/typography'), require('@tailwindcss/line-clamp')],
};
